package kr.edcan.launcher.Adapter;

import android.content.Context;
import android.content.pm.ResolveInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import kr.edcan.launcher.R;

/**
 * Created by kimok_000 on 2016-02-12.
 */
public class AppListAdapter extends ArrayAdapter<ResolveInfo> {
    private LayoutInflater mInflater;
    private Context context;

    public AppListAdapter(Context context, List<ResolveInfo> object) {
        super(context, 0, object);
        this.context = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        View view = null;
        if (v == null) {
            view = mInflater.inflate(R.layout.item_main, null);
        } else {
            view = v;
        }
        final ResolveInfo data = this.getItem(position);
        if (data != null) {
            ImageView appIcon = (ImageView) view.findViewById(R.id.appIcon);
            TextView appName = (TextView) view.findViewById(R.id.appName);

            appIcon.setImageDrawable(data.loadIcon(context.getPackageManager()));
            appName.setText(data.loadLabel(context.getPackageManager()));
        }
        return view;
    }
}
