package kr.edcan.launcher.Activity;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import kr.edcan.launcher.Adapter.AppListAdapter;
import kr.edcan.launcher.R;

public class MainActivity extends AppCompatActivity {
    private Intent mainIntent;
    private List<ResolveInfo> pkgAppList;
    private AppListAdapter appListAdapter;
    private GridView gridView;
    private EditText edit;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setDefault();
    }

    private void removeApp(String appName) {
        Intent intent = new Intent(Intent.ACTION_DELETE)
                .setData(Uri.parse("package:" + appName));

        startActivity(intent);
    }

    private void setDefault() {
        gridView = (GridView) findViewById(R.id.appList);
        edit = (EditText) findViewById(R.id.edit);
        button = (Button) findViewById(R.id.button);

        mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        pkgAppList = getPackageManager().queryIntentActivities(mainIntent, 0);

        setView(pkgAppList);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<ResolveInfo> temp = new ArrayList<ResolveInfo>();

                for (int i = 0; i < pkgAppList.size(); i++) {
                    ResolveInfo data = pkgAppList.get(i);
                    if (data.loadLabel(getPackageManager()).toString().contains(edit.getText().toString().trim())) {
                        temp.add(data);
                    } else continue;
                }

                setView(temp);
                Toast.makeText(getApplicationContext(),temp.size()+"개의 검색 결과", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setView(final List<ResolveInfo> datas) {
        appListAdapter = new AppListAdapter(getApplicationContext(), datas);
        gridView.setAdapter(appListAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ResolveInfo data = datas.get(position);
                Intent launch = new Intent();

                launch.setClassName(data.activityInfo.packageName, data.activityInfo.name);
                startActivity(launch);
            }
        });

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final ResolveInfo data = datas.get(position);

                new MaterialDialog.Builder(MainActivity.this)
                        .title("앱 삭제")
                        .content(data.loadLabel(getPackageManager()) + "을(를) 삭제하시겠습니까?")
                        .negativeText("취소")
                        .positiveText("삭제")
                        .contentColorRes(R.color.text)
                        .titleColorRes(R.color.text)
                        .backgroundColorRes(R.color.white)
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {

                            }
                        })
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                removeApp(data.activityInfo.packageName);
                            }
                        }).show();

                return true;
            }
        });
    }
}